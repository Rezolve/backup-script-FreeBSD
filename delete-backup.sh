#!/bin/sh

# Find and delete backup for 7 days

find /hdd2/backup/backupd -type f -Btime +7 -delete
sleep 5
echo "Backup deleted."
#!/bin/sh
# backup important directories except home
echo 'Started copying'
DATE=`date +%Y.%m.%d`
 echo 'Started copying the root partition'
dump -0 -L -f - / | gzip -9  >  /hdd2/backup/backupd/root.${DATE}.img.gz
 echo 'The root partition is copied, waiting 5 seconds'
sleep 5
dump -0 -L -f - /usr | gzip -9  >  /hdd2/backup/backupd/usr.${DATE}.img.gz
 echo 'The usr partition is copied, waiting 5 seconds'
sleep 5
dump -0 -L -f - /var | gzip -9  >  /hdd2/backup/backupd/var.${DATE}.img.gz
 echo 'The var partition is copied. All backup tasks completed.'
#!/bin/sh 
# Selective backup of folders, files, and databases. 
# 
# Remember the value of the current DATE in the date variable. Further used 
# DATE to form the name of the archive files.
DATE=`date +%Y.%m.%d`
#
# Specify the folder where to put all the archives. Change to fit your needs.
# The specified folder must exist and be writable.
# ARCHSTORE='/path/to/archives'
# For example, use the /hdd2/backup/web/mnt folder.
ARCHSTORE='/hdd2/backup/web/mnt'

### Start of block copy of files and folders.
# Set the data path and archive name. Change as needed.
# mysite.net
PATHNAME='/usr/local/www/mysite.net'
ARCHNAME='mysite.net'
# The process of backing up a folder or file.
 echo "Started copying a directory ${PATHNAME}"
tar -czf ${ARCHSTORE}/${ARCHNAME}.${DATE}.tar.gz ${PATHNAME}
 echo "${PATHNAME} archived, waiting 5 seconds"
sleep 5
### End of copy block
 
##### End of backup folders and files

### Start of block copy of files and folders.
# Set the data path and archive name. Change as needed.
# nginx-dist
PATHNAME='/usr/local/www/nginx-dist'
ARCHNAME='nginx-dist'
# The process of backing up a folder or file.
 echo "Started copying a directory ${PATHNAME}"
tar -czf ${ARCHSTORE}/${ARCHNAME}.${DATE}.tar.gz ${PATHNAME}
 echo "${PATHNAME} archived, waiting 5 seconds"
sleep 5
### End of copy block
 
 echo "Files and folders have been backed up." 
##### End of backup folders and files

##### Backup mysql databases.
 
### Start of mysql database backup block.
# Set user name, password, database name and archive name.
# mysql
DBUSER='root'
DBLOGIN='your_password'
DBNAME='mysql'
ARCHNAME='mysql'
#
# The process of backing up the database
echo "Database backup started ${DBNAME}."
/usr/local/bin/mysqldump -u${DBUSER} -p${DBLOGIN} ${DBNAME} > ${ARCHSTORE}/${ARCHNAME}.sql
tar -czf ${ARCHSTORE}/${ARCHNAME}_sql_${DATE}.tar.gz ${ARCHSTORE}/${ARCHNAME}.sql
rm ${ARCHSTORE}/${ARCHNAME}.sql
echo "Database backup ${DBNAME} completed."
### End of mysql database backup block.
 
echo "The databases have been backed up." 
##### End of database backup.